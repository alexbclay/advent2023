use crate::{utils::Coord, Solver};

#[derive(Debug)]
pub struct Day11 {
    galaxies: Vec<Coord>,
    empty_cols: Vec<bool>,
    empty_rows: Vec<bool>,
}

impl Day11 {
    fn distance(&self, from: usize, to: usize, multi: usize) -> usize {
        let galax_1 = self.galaxies[from];
        let galax_2 = self.galaxies[to];

        let dx = (galax_1.x - galax_2.x).unsigned_abs();
        let dy = (galax_1.y - galax_2.y).unsigned_abs();

        let range_x = match galax_1.x.cmp(&galax_2.x) {
            std::cmp::Ordering::Less => galax_1.x as usize + 1..galax_2.x as usize,
            std::cmp::Ordering::Equal => galax_1.x as usize..galax_1.x as usize,
            std::cmp::Ordering::Greater => galax_2.x as usize + 1..galax_1.x as usize,
        };

        let expanded_x = self.empty_cols[range_x].iter().filter(|col| **col).count();

        let range_y = match galax_1.y.cmp(&galax_2.y) {
            std::cmp::Ordering::Less => galax_1.y as usize + 1..galax_2.y as usize,
            std::cmp::Ordering::Equal => galax_1.y as usize..galax_1.y as usize,
            std::cmp::Ordering::Greater => galax_2.y as usize + 1..galax_1.y as usize,
        };

        let expanded_y = self.empty_rows[range_y].iter().filter(|col| **col).count();

        dx + dy + expanded_x * (multi - 1) + expanded_y * (multi - 1)
    }
}

impl Solver for Day11 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut galaxies = vec![];
        let mut empty_cols = vec![];
        let mut empty_rows = vec![];

        for (row, line) in input.lines().enumerate() {
            let mut row_is_empty = true;
            for (col, ch) in line.chars().enumerate() {
                if empty_cols.len() <= col {
                    empty_cols.push(true)
                }
                if ch == '#' {
                    row_is_empty = false;
                    empty_cols[col] = false;
                    galaxies.push(Coord::new(col, row));
                }
            }
            empty_rows.push(row_is_empty);
        }

        Ok(Box::new(Day11 {
            galaxies,
            empty_cols,
            empty_rows,
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut total = 0;
        for x in 0..self.galaxies.len() - 1 {
            for y in x + 1..self.galaxies.len() {
                total += self.distance(x, y, 2);
            }
        }
        Ok(total)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut total = 0;
        for x in 0..self.galaxies.len() - 1 {
            for y in x + 1..self.galaxies.len() {
                total += self.distance(x, y, 1_000_000);
            }
        }
        Ok(total)
    }
}
