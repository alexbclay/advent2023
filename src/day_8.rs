use std::collections::HashMap;

use crate::Solver;

#[derive(Debug)]
enum Dir {
    Left,
    Right,
}

enum ParseState {
    Key,
    First,
    Second,
}

pub struct Day8 {
    directions: Vec<Dir>,
    network: HashMap<String, (String, String)>,
    start_nodes: Vec<String>,
}

/// Gets the greatest common divisor of two numbers
/// ```
/// use advent2023::day_8::gcd;
/// assert_eq!(gcd(48, 18), 6);
/// ```
pub fn gcd(a: usize, b: usize) -> usize {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

pub fn lcm(a: usize, b: usize) -> usize {
    let divisor = gcd(a, b);

    (a * b) / divisor
}

impl Day8 {
    fn follow_dirs(&self, start_node: &str) -> usize {
        let mut cur_key = start_node;
        let mut pc = 0;
        let mut steps = 0;
        while let Some((first, second)) = self.network.get(cur_key) {
            if cur_key == "ZZZ" {
                break;
            }
            cur_key = match self.directions[pc] {
                Dir::Left => first,
                Dir::Right => second,
            };
            steps += 1;
            pc += 1;
            if pc == self.directions.len() {
                pc = 0;
            }
        }
        steps
    }

    // things I've discovered about the input:
    // - there is a single end state for each start state
    // - there is a loop for each end state that is consistent
    // - the time to the first end state is the same time as the loop itself
    fn find_loop(&self, start_node: &str) -> usize {
        let mut cur_key = start_node;
        let mut pc = 0;
        let mut steps = 0;
        // saved state is (pc, node) -> when it was seen in the sequence
        let mut saved_state: HashMap<(usize, &str), usize> = HashMap::new();
        let mut loop_state = None;
        let mut first_final = 0;
        loop {
            let state = (pc, cur_key);
            if cur_key.to_string().pop().expect("Should exist!") == 'Z' {
                if loop_state.is_none() {
                    first_final = steps;
                } else {
                    return steps - first_final;
                }
            }

            // we have already visited this state
            if saved_state.get(&state).is_some() {
                // first time visit
                if loop_state.is_none() {
                    loop_state = Some(state);
                }
            } else {
                // store this state
                saved_state.insert((pc, cur_key), steps);
            }

            // follow the directions
            let (first, second) = self.network.get(cur_key).expect("Missing node!");
            cur_key = match self.directions[pc] {
                Dir::Left => first,
                Dir::Right => second,
            };
            steps += 1;
            pc += 1;
            if pc == self.directions.len() {
                pc = 0;
            }
        }
    }
}

impl Solver for Day8 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut lines = input.lines();
        let directions = lines
            .next()
            .expect("No first line!")
            .chars()
            .map(|ch| match ch {
                'L' => Dir::Left,
                'R' => Dir::Right,
                _ => panic!("UNEXPECTED CHARACTER: {}", ch),
            })
            .collect();

        let (network, start_nodes) = lines.fold(
            (HashMap::new(), vec![]),
            |(mut map, mut start_nodes), cur| {
                if cur.is_empty() {
                    return (map, start_nodes);
                }
                let mut state = ParseState::Key;
                let mut key = String::new();
                let mut first = String::new();
                let mut second = String::new();
                let mut last_key_char: char = '_';
                for ch in cur.chars() {
                    match ch {
                        letter @ 'A'..='Z' => match state {
                            ParseState::Key => {
                                key.push(letter);
                                last_key_char = letter
                            }
                            ParseState::First => first.push(letter),
                            ParseState::Second => second.push(letter),
                        },
                        '=' => state = ParseState::First,
                        ',' => state = ParseState::Second,
                        _ => (),
                    }
                }
                if last_key_char == 'A' {
                    start_nodes.push(key.clone());
                }

                map.insert(key, (first, second));

                (map, start_nodes)
            },
        );
        Ok(Box::new(Day8 {
            directions,
            network,
            start_nodes,
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self.follow_dirs("AAA"))
    }

    fn part_two(&self) -> Result<usize, &str> {
        let periods: Vec<usize> = self
            .start_nodes
            .iter()
            .map(|node| self.find_loop(node))
            .collect();

        // find the least common multiple of all the loop periods
        let result = periods
            .iter()
            .skip(1)
            .fold(periods[0], |prev, cur| lcm(prev, *cur));

        Ok(result)
    }
}
