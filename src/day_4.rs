use crate::Solver;
use std::collections::HashSet;

pub struct Day4 {
    cards: Vec<Card>,
}

#[derive(Debug)]
struct Card {
    winning_nums: HashSet<usize>,
    my_nums: HashSet<usize>,
    my_winning_nums: HashSet<usize>,
}
impl Card {
    fn new() -> Self {
        Card {
            winning_nums: HashSet::new(),
            my_nums: HashSet::new(),
            my_winning_nums: HashSet::new(),
        }
    }
}

#[derive(Debug, PartialEq)]
enum ParseState {
    CardName,
    WinningNums,
    MyNums,
}

impl Solver for Day4 {
    fn from_input(input: &str) -> Result<Box<Self>, String> {
        let cards = input
            .lines()
            .map(|line| {
                // each line is a card
                // state is:
                //
                // list of unparsed digits,
                // and the card we're building up
                println!("LINE = {}", line);
                let (_, digits, mut card): (ParseState, Vec<char>, Card) = line.chars().fold(
                    (ParseState::CardName, vec![], Card::new()),
                    |(mut state, mut cur_digits, mut card), ch| {
                        match ch {
                            ':' => {
                                state = ParseState::WinningNums;
                            }
                            '|' => {
                                state = ParseState::MyNums;
                                if !cur_digits.is_empty() {
                                    let num =
                                        cur_digits.iter().collect::<String>().parse().unwrap();
                                    card.winning_nums.insert(num);
                                    cur_digits = vec![];
                                }
                            }
                            digit @ '0'..='9' => {
                                if state != ParseState::CardName {
                                    cur_digits.push(digit)
                                }
                            }
                            _ => {
                                if !cur_digits.is_empty() {
                                    let num =
                                        cur_digits.iter().collect::<String>().parse().unwrap();
                                    match state {
                                        ParseState::CardName => {}
                                        ParseState::WinningNums => {
                                            card.winning_nums.insert(num);
                                        }
                                        ParseState::MyNums => {
                                            card.my_nums.insert(num);
                                            if card.winning_nums.contains(&num) {
                                                card.my_winning_nums.insert(num);
                                            }
                                        }
                                    };
                                    cur_digits = vec![];
                                }
                            }
                        };
                        (state, cur_digits, card)
                    },
                );

                // add the last number
                let num = digits.iter().collect::<String>().parse().unwrap();

                card.my_nums.insert(num);
                if card.winning_nums.contains(&num) {
                    card.my_winning_nums.insert(num);
                }

                println!("CARD: {:?}", card);
                card
            })
            .collect();

        Ok(Box::new(Day4 { cards }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self.cards.iter().fold(0, |sum, card| {
            if card.my_winning_nums.is_empty() {
                sum
            } else if card.my_winning_nums.len() == 1 {
                sum + 1
            } else {
                sum + (1 << (card.my_winning_nums.len() - 1))
            }
        }))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self
            .cards
            .iter()
            .enumerate()
            .fold(
                (0, vec![0]),
                |(sum, carry_fwd): (usize, Vec<usize>), (idx, card)| {
                    let wins = card.my_winning_nums.len();
                    let cards = 1 + carry_fwd[0];
                    println!("---");
                    println!("Card {}: WINS: {} CARDS: {}", idx + 1, wins, cards);
                    let mut updates = vec![cards; wins];
                    // drop the first carried in value, add the number of new cards to the new carry fwd
                    carry_fwd
                        .iter()
                        .skip(1)
                        .enumerate()
                        .for_each(|(idx, carry)| {
                            if updates.len() >= idx {
                                updates.push(0);
                            }
                            updates[idx] += carry;
                        });
                    (sum + cards, updates)
                },
            )
            .0)
    }
}
