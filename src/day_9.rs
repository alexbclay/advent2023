use crate::Solver;

#[derive(Debug)]
pub struct Day9 {
    sequences: Vec<Vec<isize>>,
}

fn predict_sequence(sequence: &[isize]) -> isize {
    let mut diffs = vec![];
    let mut all_same = true;
    let mut have_prev = false;
    let mut prev = 0;
    let mut last = 0;
    for idx in 1..sequence.len() {
        last = sequence[idx];
        let cur_diff = sequence[idx] - sequence[idx - 1];
        diffs.push(cur_diff);
        if !have_prev {
            prev = cur_diff;
            have_prev = true;
        } else if cur_diff != prev {
            all_same = false;
        }
    }
    if all_same {
        last + prev
    } else {
        last + predict_sequence(&diffs)
    }
}

fn predict_sequence_backwards(sequence: &[isize]) -> isize {
    let mut diffs = vec![];
    let mut all_same = true;
    let mut have_prev = false;
    let mut prev = 0;
    for idx in 1..sequence.len() {
        let cur_diff = sequence[idx] - sequence[idx - 1];
        diffs.push(cur_diff);
        if !have_prev {
            prev = cur_diff;
            have_prev = true;
        } else if cur_diff != prev {
            all_same = false;
        }
    }
    if all_same {
        sequence[0] - prev
    } else {
        sequence[0] - predict_sequence_backwards(&diffs)
    }
}

impl Solver for Day9 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let sequences = input
            .lines()
            .map(|line| {
                line.split_ascii_whitespace()
                    .map(|num| num.parse().expect("Should have been a number"))
                    .collect()
            })
            .collect();

        Ok(Box::new(Day9 { sequences }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .sequences
            .iter()
            .map(|seq| predict_sequence(seq))
            .sum::<isize>() as usize)
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self
            .sequences
            .iter()
            .map(|seq| predict_sequence_backwards(seq))
            .sum::<isize>() as usize)
    }
}
