use std::collections::{HashMap, HashSet};

use crate::Solver;

pub struct Day3 {
    // raw 2d matrix of characters
    char_arr: Vec<Vec<char>>,
}

#[derive(Eq, PartialEq, Hash, Copy, Clone, Debug)]
struct Coord {
    line: usize,
    ch: usize,
}

impl Solver for Day3 {
    fn from_input(input: &str) -> Result<Box<Self>, String> {
        let char_arr: Vec<Vec<char>> = input
            .lines()
            .map(|line| -> Vec<char> { line.chars().collect() })
            .collect();

        Ok(Box::new(Day3 { char_arr }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        // array max values
        let max_lines = self.char_arr.len();
        let max_chars = self.char_arr[0].len();
        // loop through each line, and each character
        // keep track of which
        let result: usize = self
            .char_arr
            .iter()
            .enumerate()
            .fold(0, |sum, (line_idx, line)| {
                let mut processing: (Vec<char>, bool, usize) = line.iter().enumerate().fold(
                    (vec![], false, 0),
                    |(mut digits, mut symbol_adjacent, mut line_sum), (ch_idx, ch)| {
                        match ch {
                            digit @ '0'..='9' => {
                                digits.push(*digit);
                                // check for adjacent symbols to this digit
                                // account for first and last lines
                                let min_line = if line_idx == 0 {
                                    line_idx
                                } else {
                                    line_idx - 1
                                };
                                let max_line = if line_idx == max_lines - 1 {
                                    line_idx
                                } else {
                                    line_idx + 1
                                };
                                for adjacent_row in min_line..=max_line {
                                    let min_ch = if ch_idx == 0 { ch_idx } else { ch_idx - 1 };
                                    let max_ch = if ch_idx == max_chars - 1 {
                                        ch_idx
                                    } else {
                                        ch_idx + 1
                                    };
                                    for adjacent_col in min_ch..=max_ch {
                                        let val = self.char_arr[adjacent_row][adjacent_col];
                                        // would this be better as an `if` ?
                                        match val {
                                            _digit @ '0'..='9' => {}
                                            '.' => {}
                                            _ => {
                                                symbol_adjacent = true;
                                            }
                                        }
                                    }
                                }
                            }
                            _ => {
                                // any non-digit character ends the current number
                                if !digits.is_empty() {
                                    // if any of this number's digits were adjacent, then add it to the total
                                    if symbol_adjacent {
                                        let num: usize =
                                            digits.iter().collect::<String>().parse().unwrap();
                                        line_sum += num
                                    }
                                    // reset the current number trackers
                                    digits = vec![];
                                    symbol_adjacent = false;
                                }
                            }
                        };
                        (digits, symbol_adjacent, line_sum)
                    },
                );
                // collect the last number of the line if there was one and it was next to a symbol
                if !processing.0.is_empty() && processing.1 {
                    let num: String = processing.0.iter().collect();

                    processing.2 += num.parse::<usize>().unwrap();
                }
                // the result is the sum of all the numbers we found
                sum + processing.2
            });

        Ok(result)
    }

    fn part_two(&self) -> Result<usize, &str> {
        // array max values
        let max_lines = self.char_arr.len();
        let max_chars = self.char_arr[0].len();
        // loop through each line, and each character
        // keep track of which "gear" coordinates were adjacent to the number
        // then keep a map of coordinates to a list of adjacent numbers
        // once we're done collecting the map, we can pick out the gears that were adjacent to exactly 2 numbers
        let total_map: HashMap<Coord, Vec<usize>> = self.char_arr.iter().enumerate().fold(
            HashMap::new(),
            |mut gear_vals, (line_idx, line)| {
                // state is: (digits saving up to turn to number, coodinates of adjacent gears, mapping of gear coordinate to numbers)
                let processing: (Vec<char>, HashSet<Coord>) = line.iter().enumerate().fold(
                    (vec![], HashSet::new()),
                    |(mut digits, mut adjacent_gears), (ch_idx, ch)| {
                        match ch {
                            digit @ '0'..='9' => {
                                digits.push(*digit);
                                // check for adjacent '*' symbol to this digit
                                // account for first and last lines
                                let min_line = if line_idx == 0 {
                                    line_idx
                                } else {
                                    line_idx - 1
                                };
                                let max_line = if line_idx == max_lines - 1 {
                                    line_idx
                                } else {
                                    line_idx + 1
                                };
                                for adjacent_row in min_line..=max_line {
                                    let min_ch = if ch_idx == 0 { ch_idx } else { ch_idx - 1 };
                                    let max_ch = if ch_idx == max_chars - 1 {
                                        ch_idx
                                    } else {
                                        ch_idx + 1
                                    };
                                    for adjacent_col in min_ch..=max_ch {
                                        let val = self.char_arr[adjacent_row][adjacent_col];
                                        if val == '*' {
                                            adjacent_gears.insert(Coord {
                                                line: adjacent_row,
                                                ch: adjacent_col,
                                            });
                                        }
                                    }
                                }
                            }
                            _ => {
                                // any non-digit character ends the current number
                                if !digits.is_empty() {
                                    // if any of this number's digits were adjacent to some gears, then add it to the total
                                    if !adjacent_gears.is_empty() {
                                        // parse the number
                                        let num: usize =
                                            digits.iter().collect::<String>().parse().unwrap();
                                        // add it to the list for each gear found
                                        adjacent_gears.iter().for_each(|gear| {
                                            gear_vals
                                                .entry(*gear)
                                                .and_modify(|entry| entry.push(num))
                                                .or_insert(vec![num]);
                                        });
                                    }
                                    // reset the current number trackers
                                    digits = vec![];
                                    adjacent_gears = HashSet::new();
                                }
                            }
                        };
                        (digits, adjacent_gears)
                    },
                );
                // collect the last number of the line if there was one and it was next to a symbol
                if !processing.0.is_empty() && !processing.1.is_empty() {
                    let num: usize = processing.0.iter().collect::<String>().parse().unwrap();
                    // add it to the list for each gear found
                    processing.1.iter().for_each(|gear| {
                        gear_vals
                            .entry(*gear)
                            .and_modify(|entry| entry.push(num))
                            .or_insert(vec![num]);
                    });
                }
                // return the map of gear coords to their adjacent numbers
                gear_vals
            },
        );

        Ok(total_map.values().fold(0, |sum, value| {
            if value.len() == 2 {
                sum + value.iter().product::<usize>()
            } else {
                sum
            }
        }))
    }
}
