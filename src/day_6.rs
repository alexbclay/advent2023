use crate::Solver;

#[derive(Debug)]
struct Race {
    time: usize,
    distance: usize,
}

impl Race {
    fn naive_calc(&self) -> usize {
        let mut winning_count = 0;
        for x in 0..=self.time {
            let rem_time = self.time - x;
            let speed = x;
            // println!("PUSHED: {} DIST: {}", x, speed*rem_time);
            if speed * rem_time > self.distance {
                winning_count += 1;
            }
        }
        winning_count
    }

    fn quad_equation_calc(&self) -> usize {
        // solutions to equation where
        // (time - x) * x > distance
        // where x is the time the button is pushed
        // so we want all the integers where: -x^2 + tx - d > 0
        // quad equation gives 0s:
        // (-b +/- sqrt(b - 4ac))/ 2a
        // a == -1
        let b: f64 = self.time as f64;
        let c: f64 = 0f64 - (self.distance as f64);

        let numerator_1: f64 = (0f64 - b) + (b * b + 4f64 * c).sqrt();
        let numerator_2: f64 = (0f64 - b) - (b * b + 4f64 * c).sqrt();
        let denominator = -2f64;

        let mut limit_1 = numerator_1 / denominator;
        let mut limit_2 = numerator_2 / denominator;

        if limit_1 == limit_1.ceil() {
            limit_1 += 1f64;
        } else {
            limit_1 = limit_1.ceil();
        }
        if limit_2 == limit_2.floor() {
            limit_2 -= 1f64;
        } else {
            limit_2 = limit_2.floor()
        }

        (limit_2 - limit_1) as usize + 1
    }
}

pub struct Day6 {
    races: Vec<Race>,
}

impl Solver for Day6 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut lines = input.lines();
        // split by spaces, skip the first token ('Time:'), filter out empty tokens
        // TODO: switch to split_ascii_whitespace
        let times = lines
            .next()
            .unwrap()
            .split(' ')
            .skip(1)
            .filter(|tok| !tok.is_empty());
        let distances = lines
            .next()
            .unwrap()
            .split(' ')
            .skip(1)
            .filter(|tok| !tok.is_empty());
        let races: Vec<Race> = times
            .zip(distances)
            .map(|(time, distance)| Race {
                time: time.parse().unwrap(),
                distance: distance.parse().unwrap(),
            })
            .collect();

        println!("{:?}", races);
        Ok(Box::new(Day6 { races }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .races
            .iter()
            .map(|race| {
                // println!("== {:?} ==", race);
                // println!("quad: {}", race.quad_equation_calc());
                // println!("naive: {}", race.naive_calc());
                // race.naive_calc()
                race.quad_equation_calc()
            })
            .product())
    }

    fn part_two(&self) -> Result<usize, &str> {
        // combine all the numbers together
        let (time, distance) = self.races.iter().fold(
            (String::new(), String::new()),
            |(time_str, dist_str), cur_race| {
                (
                    time_str + &cur_race.time.to_string(),
                    dist_str + &cur_race.distance.to_string(),
                )
            },
        );
        let final_race = Race {
            time: time.parse().unwrap(),
            distance: distance.parse().unwrap(),
        };
        Ok(final_race.quad_equation_calc())
    }
}
