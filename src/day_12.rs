use std::{collections::HashMap, fmt::Display, vec::IntoIter};

use crate::Solver;

const DEBUG: bool = false;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Cell {
    Empty,
    Full,
    Unknown,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Pattern {
    cells: Vec<Cell>,
}

impl Pattern {
    pub fn can_match(&self, other: &Pattern) -> bool {
        self.cells.iter().zip(other.cells.iter()).all(|(s, r)| {
            matches!(
                (s, r),
                (&Cell::Full, &Cell::Full)
                    | (&Cell::Full, &Cell::Unknown)
                    | (&Cell::Unknown, &Cell::Full)
                    | (&Cell::Empty, &Cell::Empty)
                    | (&Cell::Empty, &Cell::Unknown)
                    | (&Cell::Unknown, &Cell::Empty)
            )
        })
    }
    fn push(&mut self, cell: Cell) {
        self.cells.push(cell);
    }
    fn extend(&mut self, other: Pattern) {
        self.cells.extend(other.cells);
    }

    ///```
    /// use advent2023::day_12::{Pattern,Cell};
    /// let one: Pattern = vec![Cell::Empty, Cell::Unknown, Cell::Full].into();
    /// let two: Pattern = vec![Cell::Unknown, Cell::Full, Cell::Empty].into();
    /// let one_two = one.add(&two);
    /// assert_eq!(one_two, vec![Cell::Empty, Cell::Unknown, Cell::Full,Cell::Unknown, Cell::Full, Cell::Empty].into());
    /// let two_one = two.add(&one);
    /// assert_eq!(two_one, vec![Cell::Unknown, Cell::Full, Cell::Empty,Cell::Empty, Cell::Unknown, Cell::Full].into());

    ///```
    pub fn add(&self, other: &Pattern) -> Self {
        let mut new = self.clone();
        new.extend(other.clone());
        new
    }

    pub fn len(&self) -> usize {
        self.cells.len()
    }
    pub fn is_empty(&self) -> bool {
        self.cells.is_empty()
    }
    fn cell_at(&self, index: usize) -> Cell {
        self.cells[index].clone()
    }

    ///```
    /// use advent2023::day_12::{Pattern,Cell};
    /// let pat: Pattern = vec![Cell::Unknown, Cell::Unknown, Cell::Full].into();
    /// assert!(pat.matches_first(0, true));
    /// assert!(pat.matches_first(0, false));
    /// assert!(pat.matches_first(2, true));
    /// assert!(pat.matches_first(2, false));
    /// assert!(pat.matches_first(3, true));
    /// assert!(!pat.matches_first(3, false));
    ///
    /// let pat: Pattern = vec![Cell::Full, Cell::Unknown, Cell::Full].into();
    /// assert!(pat.matches_first(pat.len(), true));
    /// assert!(!pat.matches_first(pat.len(), false));
    ///```
    pub fn matches_first(&self, n: usize, full: bool) -> bool {
        for idx in 0..n {
            match self.cells[idx] {
                Cell::Empty => {
                    if full {
                        return false;
                    }
                }
                Cell::Full => {
                    if !full {
                        return false;
                    }
                }
                Cell::Unknown => {}
            }
        }
        true
    }

    fn split_first(&self) -> Option<(Cell, Pattern)> {
        if self.cells.is_empty() {
            None
        } else {
            let mut new_cells = self.cells.clone();
            let first = new_cells.remove(0);
            Some((first, Pattern { cells: new_cells }))
        }
    }

    fn split_first_n(&self, n: usize) -> Option<(Pattern, Pattern)> {
        if self.cells.is_empty() || n > self.cells.len() {
            None
        } else {
            let mut split_pattern = self.cells.clone();
            let rest = split_pattern.split_off(n);
            Some((split_pattern.into(), rest.into()))
        }
    }

    fn fill(&self, cell_type: Cell) -> Self {
        Pattern {
            cells: self
                .cells
                .iter()
                .map(|c| {
                    if c == &Cell::Unknown {
                        cell_type.clone()
                    } else {
                        c.clone()
                    }
                })
                .collect(),
        }
    }
}

impl IntoIterator for Pattern {
    type Item = Cell;

    type IntoIter = IntoIter<Cell>;

    fn into_iter(self) -> Self::IntoIter {
        self.cells.into_iter()
    }
}

impl From<Vec<Cell>> for Pattern {
    fn from(value: Vec<Cell>) -> Self {
        Pattern { cells: value }
    }
}

impl Display for Pattern {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.cells
                .iter()
                .map(|cell| match cell {
                    Cell::Empty => '░',
                    Cell::Full => '█',
                    Cell::Unknown => '?',
                })
                .collect::<String>()
        )
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
struct Line {
    pattern: Pattern,
    clues: Vec<usize>,
}

impl Line {
    fn from_str(input_line: &str) -> Self {
        let mut parts = input_line.split(' ');
        let pattern = parts
            .next()
            .expect("Should have a first part")
            .chars()
            .fold(vec![], |mut pattern, ch| {
                pattern.push(match ch {
                    '#' => Cell::Full,
                    '.' => Cell::Empty,
                    '?' => Cell::Unknown,
                    _ => panic!("Unknown char! {}", ch),
                });
                pattern
            });
        let clues = parts
            .next()
            .expect("Should have a second part")
            .split(',')
            .map(|m| m.parse().expect("should parse to a number"))
            .collect();
        Line {
            pattern: pattern.into(),
            clues,
        }
    }

    fn expand_a_bunch(&self, time: usize) -> Line {
        let pattern_range = ..self.pattern.cells.len();
        let mut pattern = self.pattern.clone();
        for _ in 0..time {
            pattern.cells.extend([Cell::Unknown]);
            pattern.cells.extend_from_within(pattern_range);
        }

        let clue_range = ..self.clues.len();
        let mut clues = self.clues.clone();
        for _ in 0..time {
            clues.extend_from_within(clue_range);
        }

        Line { pattern, clues }
    }

    // fn find_solutions(&self) -> usize {
    //     println!("FIND SOLUTIONS TO: {} {:?}", self, self.clues);
    //     let mut queue = vec![(0, self.pattern.clone())];
    //     let mut ops = 0;
    //     let mut solutions: HashSet<Pattern> = HashSet::new();
    //     while let Some((depth, next)) = queue.pop() {
    //         if depth > 7 {
    //             // panic!("ACK!")
    //         }

    //         ops += 1;
    //         let prefix: String = (0..depth).map(|_| " . ").collect();

    //         if DEBUG {
    //             println!("{}checking {}", prefix, next)
    //         }

    //         // validate if this pattern is even possible
    //         match self.validate_incomplete(&next) {
    //             ValidationResult::Unknown => {
    //                 // do nothing, continue checking
    //             }
    //             ValidationResult::Invalid => {
    //                 // invalid! end this line of inquiry
    //                 continue;
    //             }
    //             ValidationResult::ValidAllFull => {
    //                 // the rest of them have to be full
    //                 queue.push((depth + 1, next.fill_pattern(Cell::Full)));
    //                 continue;
    //             }
    //             ValidationResult::ValidAllEmpty => {
    //                 // the rest have to be empty
    //                 queue.push((depth + 1, next.fill_pattern(Cell::Empty)));
    //                 continue;
    //             }
    //             ValidationResult::Valid => {
    //                 if DEBUG {
    //                     println!("{}VALID SOLUTION", prefix);
    //                 }
    //                 solutions.insert(next);
    //                 continue;
    //             }
    //         }

    //         // find the first unknown cell, and queue up the two possibilities
    //         if let Some(unknown_idx) =
    //             next.into_iter()
    //                 .enumerate()
    //                 .fold(None, |idx, (cur_idx, cur_cell)| {
    //                     if idx.is_some() {
    //                         idx
    //                     } else if cur_cell == Cell::Unknown {
    //                         Some(cur_idx)
    //                     } else {
    //                         None
    //                     }
    //                 })
    //         {
    //             let mut with_full = next.clone();
    //             with_full.set_cell(unknown_idx, Cell::Full);
    //             queue.push((depth + 1, with_full));
    //             let mut with_empty = next.clone();
    //             with_empty.set_cell(unknown_idx, Cell::Empty);
    //             queue.push((depth + 1, with_empty));
    //         }
    //     }
    //     if DEBUG {
    //         println!("Took {} ops", ops);
    //         for s in solutions.iter() {
    //             println!("{}", s);
    //         }
    //     }
    //     solutions.len()
    // }

    // fn validate_incomplete(&self, test_pattern: &Pattern) -> ValidationResult {
    //     // get the clues for the test pattern
    //     let (mut test_clues, final_clue, unknown_count) = test_pattern.into_iter().fold(
    //         (vec![], 0, 0),
    //         |(mut clues, mut cur_seq, mut unknown_count), cell| {
    //             match cell {
    //                 Cell::Empty => {
    //                     if cur_seq != 0 {
    //                         clues.push(cur_seq);
    //                     }
    //                     cur_seq = 0;
    //                 }
    //                 Cell::Unknown => {
    //                     if cur_seq != 0 {
    //                         clues.push(cur_seq);
    //                     }
    //                     cur_seq = 0;
    //                     unknown_count += 1;
    //                 }
    //                 Cell::Full => {
    //                     cur_seq += 1;
    //                 }
    //             }
    //             (clues, cur_seq, unknown_count)
    //         },
    //     );
    //     if final_clue != 0 {
    //         test_clues.push(final_clue);
    //     }

    //     // make sure the clues can still work
    //     if unknown_count == 0 {
    //         // there are no unknowns, so we can validate 100%
    //         if self.clues == test_clues {
    //             return ValidationResult::Valid;
    //         } else {
    //             return ValidationResult::Invalid;
    //         }
    //     }
    //     let filled_total: usize = self.clues.iter().sum();
    //     let test_total: usize = test_clues.iter().sum();
    //     if filled_total < test_total {
    //         // too many filled in!
    //         return ValidationResult::Invalid;
    //     }
    //     if filled_total == test_total && self.clues.len() != test_clues.len() {
    //         // same number of squares filled in, but the clues are different
    //         return ValidationResult::Invalid;
    //     }

    //     if self.clues.len() == test_clues.len() {
    //         // same number of clues, so all clues MUST be less than or equal to their counterparts
    //         let all_fit = self.clues.iter().zip(test_clues.iter()).all(|(c, t)| {
    //             if unknown_count > 0 {
    //                 t <= c
    //             } else {
    //                 t == c
    //             }
    //         });
    //         if !all_fit {
    //             return ValidationResult::Invalid;
    //         }
    //     }
    //     if test_total > filled_total {
    //         // println!("!! TOO MANY SQUARES!");
    //         return ValidationResult::Invalid;
    //     } else if test_total == filled_total {
    //         // println!(">> all remaining should be empty");
    //         return ValidationResult::ValidAllEmpty;
    //     } else if test_total + unknown_count == filled_total {
    //         // println!(">> all remaining should be filled");
    //         // FIXME: this name is bad
    //         return ValidationResult::ValidAllFull;
    //     }

    //     ValidationResult::Unknown
    // }

    // fn validate_complete(&self, test_pattern: &Pattern) -> bool {
    //     // get the clues for the test pattern
    //     let (mut test_clues, final_clue) =
    //         test_pattern
    //             .into_iter()
    //             .fold((vec![], 0), |(mut clues, mut cur_seq), cell| {
    //                 match cell {
    //                     Cell::Empty => {
    //                         if cur_seq != 0 {
    //                             clues.push(cur_seq);
    //                         }
    //                         cur_seq = 0;
    //                     }
    //                     Cell::Full => {
    //                         cur_seq += 1;
    //                     }
    //                     Cell::Unknown => panic!("TRIED TO TEST INCOMPLETE!"),
    //                 }
    //                 (clues, cur_seq)
    //             });
    //     if final_clue != 0 {
    //         test_clues.push(final_clue);
    //     }
    //     println!("REAL CLUES: {:?}", self.clues);
    //     println!("TEST CLUES: {:?}", test_clues);
    //     self.clues == test_clues
    // }

    // fn validate_old(&self) -> ValidationResult {
    //     // if the line has no unknowns, we can get the matching clues and see if it's the same as the given clue
    //     let mut as_is = vec![];
    //     let mut cur_as_is = 0;
    //     let mut all_empty = vec![];
    //     let mut cur_seq_empty = 0;
    //     let mut all_full = vec![];
    //     let mut cur_seq_full = 0;
    //     let mut has_unknown = false;
    //     for cell in self.pattern.iter() {
    //         match cell {
    //             Cell::Empty => {
    //                 if cur_seq_empty != 0 {
    //                     all_empty.push(cur_seq_empty)
    //                 }
    //                 if cur_seq_full != 0 {
    //                     all_full.push(cur_seq_full)
    //                 }
    //                 if cur_as_is != 0 {
    //                     as_is.push(cur_as_is);
    //                 }
    //                 cur_seq_empty = 0;
    //                 cur_seq_full = 0;
    //                 cur_as_is = 0;
    //             }
    //             Cell::Full => {
    //                 cur_seq_empty += 1;
    //                 cur_seq_full += 1;
    //                 cur_as_is += 1;
    //             }
    //             Cell::Unknown => {
    //                 if cur_seq_empty != 0 {
    //                     all_empty.push(cur_seq_empty)
    //                 }
    //                 cur_seq_empty = 0;
    //                 cur_seq_full += 1;
    //                 has_unknown = true;
    //             }
    //         }
    //     }

    //     if cur_seq_empty != 0 {
    //         all_empty.push(cur_seq_empty);
    //     }
    //     if all_empty.is_empty() {
    //         all_empty.push(0);
    //     }
    //     if all_empty == self.clues {
    //         // the clues are exactly equal, then it's valid when all unknowns are empty
    //         return ValidationResult::ValidAllEmpty;
    //     }
    //     if cur_seq_full != 0 {
    //         all_full.push(cur_seq_full);
    //     }
    //     if all_full.is_empty() {
    //         all_full.push(0);
    //     }
    //     if all_full == self.clues {
    //         // the clues are exactly equal, then it's valid when all unknowns are full
    //         return ValidationResult::ValidAllFull;
    //     }

    //     if !has_unknown {
    //         // all the unknowns were tested and none worked, so this is invalid
    //         return ValidationResult::Invalid;
    //     }

    //     ValidationResult::Unknown

    // }

    // fn fill_in(&self, cell_type: Cell) -> Self {
    //     let mut filled = self.clone();
    //     filled.pattern = filled
    //         .pattern
    //         .into_iter()
    //         .map(|cell| match cell {
    //             Cell::Unknown => cell_type.clone(),
    //             _ => cell,
    //         })
    //         .collect();
    //     filled
    // }

    // fn get_possible_solutions(&self, depth: usize) -> HashSet<Self> {
    //     let prefix: String = (0..depth).map(|_| " . ").collect();
    //     if DEBUG {
    //     println!(
    //         "{} >> GETTING SOLUTIONS: {}",
    //         prefix,
    //         get_pattern_display(&self.pattern)
    //     );}
    //     match self.validate_old() {
    //         ValidationResult::Unknown => {
    //             // keep recursing
    //             if DEBUG {println!("{} UNKNOWN", prefix);}
    //         }
    //         ValidationResult::Invalid => {
    //             // no possible solutions, return empty set
    //             if DEBUG {println!("{} INVALID", prefix);}
    //             return HashSet::new();
    //         }
    //         ValidationResult::ValidAllFull => {
    //             if DEBUG {println!("{} VALID FULL", prefix);}
    //             return HashSet::from([self.fill_in(Cell::Full)])},
    //         ValidationResult::ValidAllEmpty => {
    //             if DEBUG {println!("{} VALID EMPTY", prefix);}
    //             return HashSet::from([self.fill_in(Cell::Empty)])
    //         },
    //     }

    //     let mut options = HashSet::new();
    //     for (idx, cell) in self.pattern.iter().enumerate() {
    //         if let Cell::Unknown = cell {
    //             let mut child_on = self.clone();
    //             child_on.pattern[idx] = Cell::Full;
    //             options.extend(child_on.get_possible_solutions(depth + 1));

    //             let mut child_off = self.clone();
    //             child_off.pattern[idx] = Cell::Empty;
    //             options.extend(child_off.get_possible_solutions(depth + 1));
    //             break;
    //         }
    //     }
    //     options
    // }
}

impl Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.pattern // self.clues.iter().fold(String::new(), |mut out, clue| {
                         //     out.push(' ');
                         //     out.push_str(&clue.to_string());
                         //     out
                         // })
        )
    }
}

#[derive(Debug)]
pub struct Day12 {
    lines: Vec<Line>,
}

impl Solver for Day12 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let lines = input.lines().map(Line::from_str).collect();

        Ok(Box::new(Day12 { lines }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut memo = HashMap::new();
        Ok(self
            .lines
            .iter()
            .map(|l| {
                println!("Solving: {} {:?}", l.pattern, l.clues);
                let count = solve_it(0, l, &mut memo);
                println!("COUNT: {}", count);
                count
            })
            .sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let updated = Day12 {
            lines: self
                .lines
                .iter()
                .map(|old_line| old_line.expand_a_bunch(4))
                .collect(),
        };

        println!("UPDATED");
        for line in updated.lines.iter() {
            println!("{}", line);
        }
        Ok(updated.part_one().expect("Should have a solution!"))
        // Err("also meh")
    }
}

fn solve_it(depth: usize, puzzle: &Line, memo: &mut HashMap<Line, usize>) -> usize {
    let prefix: String = (0..depth).map(|_| " . ").collect();
    if let Some(solutions) = memo.get(puzzle) {
        if DEBUG {
            println!(
                "{} <><> MEMOIZED! <><> ALREADY SOLVED: {} - {:?} ({})",
                prefix,
                puzzle.pattern,
                puzzle.clues,
                solutions
            );
        }
        return *solutions;
    }
    if DEBUG {
        println!(
            "{} DEPTH {}: Checking [{}]   {:?}",
            prefix, depth, puzzle.pattern, puzzle.clues
        );
    }
    if puzzle.clues.is_empty() {
        if DEBUG {
            println!("{} No more clues, so the rest must be empty-able", prefix);
        }
        if puzzle.pattern.matches_first(puzzle.pattern.len(), false) {
            if DEBUG {
                println!("{} Rest can be empty. Soln!", prefix);
            }
            let solutions = 1;
            memo.insert(puzzle.clone(), solutions);
            return solutions;
        } else {
            if DEBUG {
                println!("{} Rest CANNOT be empty.  No solution.", prefix);
            }
            memo.insert(puzzle.clone(), 0);
            return 0;
        }
    }

    if puzzle.pattern.is_empty() {
        if DEBUG {
            println!("{} Nothing left", prefix);
        }
        memo.insert(puzzle.clone(), 0);
        return 0;
    }

    let needed_space = puzzle.clues.iter().sum::<usize>() + puzzle.clues.len() - 1;
    if needed_space > puzzle.pattern.len() {
        if DEBUG {
            println!("{} Not enough room left", prefix);
        }
        memo.insert(puzzle.clone(), 0);
        return 0;
    }

    if needed_space == puzzle.pattern.len() {
        if DEBUG {
            println!("{} Should be filled in", prefix);
        }
        // the rest should be filled in
        let solution: Pattern = puzzle
            .clues
            .iter()
            .map(|clue| vec![Cell::Full; *clue])
            .collect::<Vec<Vec<Cell>>>()
            .join(&Cell::Empty)
            .into();
        if DEBUG {
            println!("{} --> COMPUTED: {}", prefix, solution);
        }
        if puzzle.pattern.can_match(&solution) {
            if DEBUG {
                println!("{} --> MATCH!  : {}", prefix, puzzle.pattern);
                println!("{} matches, so soln!", prefix);
            }
            memo.insert(puzzle.clone(), 1);
            return 1;
        } else {
            if DEBUG {
                println!("{} NOT MATCH!  : {}", prefix, puzzle.pattern);
                println!("{} NO matches...", prefix);
            }
            memo.insert(puzzle.clone(), 0);
            return 0;
        }
    }

    let mut solns = 0;
    // two possibilities:

    // 1. the first clue can fit into the existing pattern
    let valid = puzzle.pattern.matches_first(puzzle.clues[0], true)
        && matches!(
            puzzle.pattern.cell_at(puzzle.clues[0]),
            Cell::Empty | Cell::Unknown
        );

    let mut first_clue = puzzle.clues.clone();
    let rest_clues = first_clue.split_off(1);

    if valid {
        let (_first_rem, rest_rem) = puzzle
            .pattern
            .split_first_n(puzzle.clues[0] + 1)
            .expect("Should be more than one element!");

        // prefix becomes full first clue, plus an empty cell
        let mut pre: Pattern = vec![Cell::Full; first_clue[0]].into();
        pre.push(Cell::Empty);

        if DEBUG {
            println!(
                "{} first clue fits, no soln yet so iter [{}][{}] -- {:?}",
                prefix, pre, rest_rem, rest_clues
            );
        }

        let rest_puzzle = Line {
            pattern: rest_rem,
            clues: rest_clues,
        };
        let with_first = solve_it(depth + 1, &rest_puzzle, memo);

        solns += with_first;
    } else if DEBUG {
        println!("{} First clue does not fit.", prefix);
    }

    // 2. there can be an empty space in the first spot, so no clue is consumed
    // FIXME: check to see if there can be an empty space
    if matches!(puzzle.pattern.cell_at(0), Cell::Empty | Cell::Unknown) {
        let without_prefix: Pattern = vec![Cell::Empty].into();
        let (_first_rem, rest_rem) = puzzle.pattern.split_first().expect("Should have elements");
        // let mut first_rem = rem.clone();
        // let rest_rem = first_rem.split_off(1);
        let rest_puzzle = Line {
            pattern: rest_rem,
            clues: puzzle.clues.clone(),
        };

        if DEBUG {
            println!(
                "{} ITER WITH SECOND [{}][{}] -- {:?}",
                prefix, without_prefix, rest_puzzle.pattern, rest_puzzle.clues
            );
        }
        let without = solve_it(depth + 1, &rest_puzzle, memo);

        solns += without;
    }

    memo.insert(puzzle.clone(), solns);
    solns
}
