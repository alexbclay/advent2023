use advent2023::{day_10, day_11, day_12, day_3, day_4, day_5, day_6, day_7, day_8, day_9, Solver};
use std::ffi::OsString;

struct Args {
    day: u8,
    input: String,
}

fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;
    let mut day: Option<u8> = None;
    let mut input: Option<String> = None;

    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Short('d') | Long("day") => {
                day = Some(parser.value()?.parse()?);
            }
            Short('i') | Long("input") => input = Some(parser.value()?.parse()?),
            _ => return Err(arg.unexpected()),
        }
    }

    match (day, input) {
        (None, None) => Err(lexopt::Error::MissingValue {
            option: Some("day".to_string()),
        }),
        (None, Some(_)) => Err(lexopt::Error::MissingValue {
            option: Some("day".to_string()),
        }),
        (Some(_), None) => Err(lexopt::Error::MissingValue {
            option: Some("input".to_string()),
        }),
        (Some(day), Some(input)) => Ok(Args { day, input }),
    }
}

fn main() -> Result<(), lexopt::Error> {
    let args = parse_args()?;

    if let Ok(content) = std::fs::read_to_string(args.input) {
        let day: Box<dyn Solver> = match args.day {
            3 => day_3::Day3::from_input(&content)?,
            4 => day_4::Day4::from_input(&content)?,
            5 => day_5::Day5::from_input(&content)?,
            6 => day_6::Day6::from_input(&content)?,
            7 => day_7::Day7::from_input(&content)?,
            8 => day_8::Day8::from_input(&content)?,
            9 => day_9::Day9::from_input(&content)?,
            10 => day_10::Day10::from_input(&content)?,
            11 => day_11::Day11::from_input(&content)?,
            12 => day_12::Day12::from_input(&content)?,
            // x => day_x::Dayx::from_input(&content)?,
            not_impl => {
                return Err(lexopt::Error::UnexpectedValue {
                    option: "day".to_string(),
                    value: OsString::from(format!("{}", not_impl)),
                })
            }
        };

        let part_1 = day.part_one()?;
        println!("Part 1: {}", part_1);

        let part_2 = day.part_two()?;
        println!("Part 2: {}", part_2);
    };

    Ok(())
}
