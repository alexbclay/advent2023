pub trait Solver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized;

    fn part_one(&self) -> Result<usize, &str> {
        Err("Not implemented yet!")
    }
    fn part_two(&self) -> Result<usize, &str> {
        Err("Not implemented yet!")
    }
}

pub mod day_10;
pub mod day_11;
pub mod day_12;
pub mod day_3;
pub mod day_4;
pub mod day_5;
pub mod day_6;
pub mod day_7;
pub mod day_8;
pub mod day_9;
pub mod utils;
