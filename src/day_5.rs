use crate::Solver;
use std::collections::HashMap;

#[derive(Debug)]
pub struct Day5 {
    seeds: Vec<usize>,
    maps: HashMap<String, (String, Vec<Mapping>)>,
}

enum ParseState {
    Seeds,
    MapName,
    MapLine,
}

impl Solver for Day5 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        // should state just be at the function scope instead of `fold`ing?
        let mut state = ParseState::Seeds;

        let (_, day) = input.lines().fold(
            (
                String::new(),
                Day5 {
                    seeds: vec![],
                    maps: HashMap::new(),
                },
            ),
            |(cur_map_name, mut day), line| {
                if line.starts_with('#') {
                    return (cur_map_name, day);
                }

                let mut next_map_name = cur_map_name;
                match state {
                    ParseState::Seeds => {
                        // first line is a list of seeds
                        let (mut nums, digits) =
                            line.chars()
                                .fold((vec![], vec![]), |(mut nums, mut digits), ch| {
                                    match ch {
                                        digit @ '0'..='9' => {
                                            digits.push(digit);
                                        }
                                        _ => {
                                            if !digits.is_empty() {
                                                let num: usize = digits
                                                    .iter()
                                                    .collect::<String>()
                                                    .parse()
                                                    .unwrap();
                                                nums.push(num);
                                                digits = vec![];
                                            }
                                        }
                                    };
                                    (nums, digits)
                                });
                        if !digits.is_empty() {
                            let num = digits.iter().collect::<String>().parse().unwrap();
                            nums.push(num);
                        };
                        day.seeds = nums;
                        state = ParseState::MapName;
                    }
                    ParseState::MapName => {
                        if !line.is_empty() {
                            // Extract the before and after names
                            let mut from = vec![];
                            let mut to = vec![];
                            let mut is_from = 0;
                            line.chars().for_each(|ch| {
                                if is_from == 3 {
                                    return;
                                }
                                if ch == '-' {
                                    is_from = if is_from == 0 { 1 } else { 2 };
                                } else if ch == ' ' {
                                    is_from = 3
                                } else if is_from == 0 {
                                    from.push(ch);
                                } else if is_from == 2 {
                                    to.push(ch);
                                }
                            });
                            next_map_name = from.iter().collect();
                            day.maps
                                .insert(from.iter().collect(), (to.iter().collect(), vec![]));
                            state = ParseState::MapLine;
                        }
                    }
                    ParseState::MapLine => {
                        if line.is_empty() {
                            state = ParseState::MapName;
                        } else {
                            // each map line is a list of numbers
                            let (mut nums, digits) = line.chars().fold(
                                (vec![], vec![]),
                                |(mut nums, mut digits), ch| {
                                    match ch {
                                        digit @ '0'..='9' => {
                                            digits.push(digit);
                                        }
                                        _ => {
                                            if !digits.is_empty() {
                                                let num: usize = digits
                                                    .iter()
                                                    .collect::<String>()
                                                    .parse()
                                                    .unwrap();
                                                nums.push(num);
                                                digits = vec![];
                                            }
                                        }
                                    };
                                    (nums, digits)
                                },
                            );
                            if !digits.is_empty() {
                                let num = digits.iter().collect::<String>().parse().unwrap();
                                nums.push(num)
                            };
                            day.maps.entry(next_map_name.clone()).and_modify(|entry| {
                                // not good error checking, but we know what the input looks like
                                entry.1.push(Mapping::new(nums[0], nums[1], nums[2]));
                            });
                        }
                    }
                }
                (next_map_name, day)
            },
        );

        Ok(Box::new(day))
    }

    fn part_one(&self) -> Result<usize, &str> {
        // start with "seed"
        let mut values = self.seeds.clone();
        let mut key = "seed".to_string();
        while let Some((next_key, mappings)) = self.maps.get(&key) {
            // run the values through the mappings
            values = values
                .iter()
                .map(|&v| {
                    for mapping in mappings {
                        let mapped = mapping.apply_single(v);
                        if mapped != v {
                            // value was changed, so return it
                            return mapped;
                        }
                    }
                    // value was not changed by any mapping, return the unchanged value
                    v
                })
                .collect();
            key = next_key.to_string();
        }
        Ok(*values.iter().min().unwrap())
    }

    fn part_two(&self) -> Result<usize, &str> {
        // start with "seed"
        println!("Making seeds");
        let (mut values, _, _) =
            self.seeds
                .iter()
                .fold((vec![], true, 0), |(mut seeds, is_start, carried), cur| {
                    if is_start {
                        (seeds, false, *cur)
                    } else {
                        seeds.push(Range::new(carried, carried + cur));
                        (seeds, true, 0)
                    }
                });
        println!("Computing transforms");
        let mut key = "seed".to_string();
        while let Some((next_key, mappings)) = self.maps.get(&key) {
            // println!();
            // println!();
            // println!(">>>>>>> {} to {} <<<<<<<<", key, next_key);
            // println!("VALS: {:?}", values);
            // println!();

            let mut new_values = vec![];
            for range in values.iter() {
                // println!("Processing: {:?}", range);
                let (unprocessed, done) = mappings.iter().fold(
                    (vec![range.clone()], vec![]),
                    |(unprocessed, mut done): (Vec<Range>, Vec<Range>), map: &Mapping| {
                        // println!("   State:");
                        // println!("     unprocessed: {:?}", unprocessed);
                        // println!("     done       : {:?}", done);
                        // println!("   Applying map : {:?}", map);
                        let mut new_unprocessed = vec![];
                        for unproc in unprocessed {
                            let (same, mapped) = map.apply(&unproc);
                            new_unprocessed.extend(same);
                            done.extend(mapped);
                        }
                        (new_unprocessed, done)
                    },
                );
                new_values.extend(unprocessed);
                new_values.extend(done);
            }
            values = new_values;
            key = next_key.to_string();
        }

        Ok(values.iter().fold(usize::MAX, |cur_min, cur_range| {
            if cur_range.min < cur_min {
                cur_range.min
            } else {
                cur_min
            }
        }))
    }
}

#[derive(Debug)]
pub struct Mapping {
    pub from: usize,
    pub to: usize,
    pub dist: usize,
}

impl Mapping {
    pub fn new(to: usize, from: usize, dist: usize) -> Self {
        Mapping { to, from, dist }
    }

    pub fn apply_single(&self, source: usize) -> usize {
        if source >= self.from && source < self.from + self.dist {
            self.to + (source - self.from)
        } else {
            source
        }
    }

    /// Applying a map can result in 4 cases:
    /// 0. range does not overlap with mapping zone: vec[input], None
    /// 1. range is entirely within mapping zone: empty vec, some[newly mapped range]
    /// 2. range contains full mapping zone: vec[smaller range, larger range], some[newly mapped range]
    /// 3. range overlaps mapping zone: vec[unmapped range], some[newly mapped range]
    /// ```
    /// use advent2023::day_5::{Range, Mapping};
    /// let mapping = Mapping::new(35,50,20);

    /// // 100% outside
    /// let range_outside_small = Range::new(0, 49);
    /// assert_eq!(mapping.apply(&range_outside_small), (vec![Range::new(0, 49)], None));
    /// let range_outside_large = Range::new(70, 1000);
    /// assert_eq!(mapping.apply(&range_outside_large), (vec![Range::new(70, 1000)], None));

    /// // 100% inside
    /// let range_equal = Range::new(50,69);
    /// assert_eq!(mapping.apply(&range_equal), (vec![], Some(Range::new(35,54))));
    /// let range_inside = Range::new(60,65);
    /// assert_eq!(mapping.apply(&range_inside), (vec![], Some(Range::new(45,50))));

    /// // range encompasses mapping
    /// let range_equal = Range::new(40,80);
    /// assert_eq!(mapping.apply(&range_equal), (vec![Range::new(40,49), Range::new(70,80)], Some(Range::new(35,54))));

    /// // range overlaps small end of mapping
    /// let range_overlap_small = Range::new(40,55);
    /// assert_eq!(mapping.apply(&range_overlap_small), (vec![Range::new(40,49)], Some(Range::new(35,40))));

    /// // range overlaps large end of mapping
    /// let range_overlap_large = Range::new(55, 75);
    /// assert_eq!(mapping.apply(&range_overlap_large), (vec![Range::new(70,75)], Some(Range::new(40,54))));

    /// // example test
    /// let example_range = Range::new(81, 95);
    /// let example_map = Mapping::new(18, 25, 70);
    /// assert_eq!(example_map.apply(&example_range), (vec![Range::new(95, 95)], Some(Range::new(74,87))));
    /// ```
    pub fn apply(&self, range: &Range) -> (Vec<Range>, Option<Range>) {
        let from_end = self.from + self.dist;
        if range.max < self.from || range.min >= from_end {
            // case 0: no overlap
            (vec![range.clone()], None)
        } else if range.min >= self.from && range.max < from_end {
            // case 1: range is entirely inside
            let new_start = range.min - self.from + self.to;
            let new_end = range.max - range.min + new_start;
            (vec![], Some(Range::new(new_start, new_end)))
        } else if range.min < self.from && range.max >= from_end {
            // case 2: mapping is entirely inside range
            (
                vec![
                    Range::new(range.min, self.from - 1),
                    Range::new(self.from + self.dist, range.max),
                ],
                Some(Range::new(self.to, (self.to + self.dist) - 1)),
            )
        } else if range.min < self.from && range.max < from_end {
            // case 3a: range overlaps small end of mapping
            let overlap = range.max - self.from;
            (
                vec![Range::new(range.min, self.from - 1)],
                Some(Range::new(self.to, self.to + overlap)),
            )
        } else if range.min <= from_end && range.max >= from_end {
            let new_start = range.min - self.from + self.to;
            (
                vec![Range::new(from_end, range.max)],
                Some(Range::new(new_start, self.to + self.dist - 1)),
            )
        } else {
            (vec![], None)
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
pub struct Range {
    pub min: usize,
    // inclusive
    pub max: usize,
}

impl Range {
    pub fn new(min: usize, max: usize) -> Self {
        Range { min, max }
    }

    /// Merges two ranges together.
    /// ```
    /// use advent2023::day_5::Range;
    /// let range_one = Range { min: 2, max: 4 };
    /// let range_two = Range { min: 3, max: 6 };
    /// let merged = range_one.merge(&range_two);
    /// assert_eq!(merged, vec![Range { min: 2, max: 6 }]);

    /// let range_three = Range { min: 2, max: 4 };
    /// let range_four = Range { min: 5, max: 10 };
    /// let merged = range_three.merge(&range_four);
    /// assert_eq!(merged, vec![Range { min: 2, max: 10 }]);

    /// assert_eq!(Range::new(7, 10).merge(&Range::new(8, 11)), vec![Range::new(7, 11)]);
    /// assert_eq!(Range::new(8, 11).merge(&Range::new(7, 10)), vec![Range::new(7, 11)]);
    /// ```
    pub fn merge(&self, other: &Self) -> Vec<Self> {
        if self.max + 1 == other.min {
            // no separating, merge
            vec![Range {
                min: self.min,
                max: other.max,
            }]
        } else if other.max + 1 == self.min {
            // no separation
            vec![Range {
                min: other.min,
                max: self.max,
            }]
        } else if self.max < other.min || self.min > other.max {
            // no overlap between ranges, so keep both in order of mins
            if self.min < other.min {
                vec![self.clone(), other.clone()]
            } else {
                vec![other.clone(), self.clone()]
            }
        } else {
            // overlaps, so choose the smallest min and the largest max
            vec![Range {
                min: if self.min < other.min {
                    self.min
                } else {
                    other.min
                },
                max: if self.max > other.max {
                    self.max
                } else {
                    other.max
                },
            }]
        }
    }
}
