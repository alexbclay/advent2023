use std::cmp::Ordering;

use crate::Solver;

const HIGH_CARD: u8 = 0;
const ONE_PAIR: u8 = 1;
const TWO_PAIR: u8 = 2;
const THREE_OF_A_KIND: u8 = 3;
const FULL_HOUSE: u8 = 4;
const FOUR_OF_A_KIND: u8 = 5;
const FIVE_OF_A_KIND: u8 = 6;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Hand {
    cards: [u8; 5],
    strength: u8,
    value: usize,
    bid: usize,
}

impl Hand {
    fn new(cards: [char; 5], bid: usize) -> Self {
        let mut converted_cards: [u8; 5] = [0; 5];
        let mut value: usize = 0;
        let mut duples: [u8; 14] = [0; 14];
        for (index, card) in cards.iter().enumerate() {
            let card_value = match card {
                digit @ '2'..='9' => *digit as u8 - b'0',
                'T' => 10,
                'J' => 11,
                'Q' => 12,
                'K' => 13,
                'A' => 14,
                _ => panic!("Got wrong card: {}", card),
            };
            converted_cards[index] = card_value;
            value = value * 14 + (card_value as usize);
            duples[card_value as usize - 1] += 1;
        }
        // find the strength of the hand
        let multi: Vec<&u8> = duples.iter().filter(|&val| *val > 1u8).collect();

        let strength = if multi.is_empty() {
            // no duplicates
            HIGH_CARD
        } else if multi.len() == 1 {
            // one duplicate: check which kind it is
            match multi[0] {
                2 => ONE_PAIR,
                3 => THREE_OF_A_KIND,
                4 => FOUR_OF_A_KIND,
                5 => FIVE_OF_A_KIND,
                _ => panic!("Wrong card count! {:?}", cards),
            }
        } else if multi.len() == 2 {
            match (multi[0], multi[1]) {
                (2, 3) => FULL_HOUSE,
                (3, 2) => FULL_HOUSE,
                (2, 2) => TWO_PAIR,
                _ => panic!("Wrong card pair count! {:?}", cards),
            }
        } else {
            panic!("Could not determine strength of hand: {:?}", cards);
        };
        Hand {
            cards: converted_cards,
            bid,
            strength,
            value,
        }
    }

    fn new_wild(cards: [char; 5], bid: usize) -> Self {
        let mut converted_cards: [u8; 5] = [0; 5];
        let mut value: usize = 0;
        let mut duples: [u8; 14] = [0; 14];
        for (index, card) in cards.iter().enumerate() {
            let card_value = match card {
                digit @ '2'..='9' => *digit as u8 - b'0',
                'T' => 10,
                // jokers are worth little individually
                'J' => 1,
                'Q' => 12,
                'K' => 13,
                'A' => 14,
                _ => panic!("Got wrong card: {}", card),
            };
            converted_cards[index] = card_value;
            value = value * 14 + (card_value as usize);
            duples[card_value as usize - 1] += 1;
        }
        if duples[0] > 0 {
            // there's a joker, so add it to the card with the most duplicates already
            let mut highest = 1;
            for x in 1..duples.len() {
                if duples[x] > duples[highest] {
                    highest = x;
                }
            }
            let jokers = duples[0];
            duples[0] = 0;
            duples[highest] += jokers;
        }
        let multi: Vec<&u8> = duples.iter().filter(|&val| *val > 1u8).collect();

        // find the strength of the hand
        let strength = if multi.is_empty() {
            // no duplicates
            HIGH_CARD
        } else if multi.len() == 1 {
            // one duplicate: check which kind it is
            match multi[0] {
                2 => ONE_PAIR,
                3 => THREE_OF_A_KIND,
                4 => FOUR_OF_A_KIND,
                5 => FIVE_OF_A_KIND,
                _ => panic!("Wrong card count! {:?}", cards),
            }
        } else if multi.len() == 2 {
            match (multi[0], multi[1]) {
                (2, 3) => FULL_HOUSE,
                (3, 2) => FULL_HOUSE,
                (2, 2) => TWO_PAIR,
                _ => panic!("Wrong card pair count! {:?}", cards),
            }
        } else {
            panic!("Could not determine strength of hand: {:?}", cards);
        };
        Hand {
            cards: converted_cards,
            bid,
            strength,
            value,
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.strength.cmp(&other.strength) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => self.value.cmp(&other.value),
        }
    }
}

#[derive(Debug)]
pub struct Day7 {
    hands: Vec<Hand>,
    wild_hands: Vec<Hand>,
}

impl Solver for Day7 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let (hands, wild_hands): (Vec<Hand>, Vec<Hand>) =
            input
                .lines()
                .fold((vec![], vec![]), |(mut hands, mut wild_hands), line| {
                    let mut tokens = line.split(' ');
                    let cards: [char; 5] = tokens
                        .next()
                        .unwrap()
                        .chars()
                        .collect::<Vec<char>>()
                        .try_into()
                        .expect("Wrong number of cards");
                    let bid: usize = tokens.next().unwrap().parse().unwrap();
                    hands.push(Hand::new(cards, bid));
                    wild_hands.push(Hand::new_wild(cards, bid));
                    (hands, wild_hands)
                });
        Ok(Box::new(Day7 { hands, wild_hands }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut sorted = self.hands.clone();
        sorted.sort_unstable();
        let solution = sorted
            .iter()
            .enumerate()
            .fold(0, |total, (idx, cur_hand)| total + (idx + 1) * cur_hand.bid);
        Ok(solution)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut sorted = self.wild_hands.clone();
        sorted.sort_unstable();
        let solution = sorted.iter().enumerate().fold(0, |total, (idx, cur_hand)| {
            // println!("{}: {:?}", idx, cur_hand);
            total + (idx + 1) * cur_hand.bid
        });
        Ok(solution)
    }
}
