use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use crate::{utils::Coord, Solver};

#[derive(Debug, PartialEq, Copy, Clone)]
enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Clone, Debug, PartialEq)]
enum Tile {
    Empty,
    Outside,
    Inside,
    Start,
    EastWest,
    NorthSouth,
    NorthWest,
    SouthWest,
    NorthEast,
    SouthEast,
}

impl Tile {
    fn to_directions(&self) -> Vec<Direction> {
        match self {
            Tile::Empty | Tile::Outside | Tile::Inside => vec![],
            Tile::Start => vec![
                Direction::North,
                Direction::East,
                Direction::South,
                Direction::West,
            ],
            Tile::EastWest => vec![Direction::East, Direction::West],
            Tile::NorthSouth => vec![Direction::North, Direction::South],
            Tile::NorthWest => vec![Direction::North, Direction::West],
            Tile::SouthWest => vec![Direction::South, Direction::West],
            Tile::NorthEast => vec![Direction::North, Direction::East],
            Tile::SouthEast => vec![Direction::South, Direction::East],
        }
    }

    fn get_neighbors(&self, coord: &Coord) -> Vec<(Direction, Coord)> {
        self.to_directions()
            .into_iter()
            .map(|direction| (direction, coord.go(&direction)))
            .collect()
    }
}

impl From<&Tile> for char {
    fn from(value: &Tile) -> Self {
        match value {
            Tile::Empty => '.',
            Tile::Outside => 'O',
            Tile::Inside => 'I',
            Tile::Start => 's',
            Tile::EastWest => '═',
            Tile::NorthSouth => '║',
            Tile::NorthWest => '╝',
            Tile::SouthWest => '╗',
            Tile::NorthEast => '╚',
            Tile::SouthEast => '╔',
        }
    }
}

impl Coord {
    fn go(&self, direction: &Direction) -> Coord {
        match direction {
            Direction::North => Coord {
                x: self.x,
                y: self.y - 1,
            },
            Direction::East => Coord {
                x: self.x + 1,
                y: self.y,
            },
            Direction::South => Coord {
                x: self.x,
                y: self.y + 1,
            },
            Direction::West => Coord {
                x: self.x - 1,
                y: self.y,
            },
        }
    }
}

pub struct Day10 {
    start_tile: Coord,
    tiles: Vec<Vec<Tile>>,
    graph: Option<HashMap<Coord, Vec<Coord>>>,
}

impl Day10 {
    fn tile_at(&self, coord: &Coord) -> Option<&Tile> {
        if let Ok(col) = TryInto::<usize>::try_into(coord.x) {
            if let Ok(row) = TryInto::<usize>::try_into(coord.y) {
                if let Some(tile_row) = self.tiles.get(row) {
                    return tile_row.get(col);
                }
            }
        }
        None
    }

    fn make_graph(&self) -> HashMap<Coord, Vec<Coord>> {
        // create the graph
        let mut graph = HashMap::new();
        self.tiles.iter().enumerate().for_each(|(y, line)| {
            line.iter().enumerate().for_each(|(x, _tile)| {
                let coord = Coord::new(x, y);
                if let Some(tile) = self.tile_at(&coord) {
                    let neighbors: Vec<Coord> = tile
                        .get_neighbors(&coord)
                        .iter()
                        .filter_map(|(from_dir, n)| {
                            // it's only a connection if the tile exists
                            if let Some(tile) = self.tile_at(n) {
                                // and it has a valid inverse connection
                                match (tile, from_dir) {
                                    (Tile::Start, _) => Some(*n),
                                    (Tile::EastWest, Direction::East | Direction::West) => Some(*n),
                                    (Tile::NorthSouth, Direction::North | Direction::South) => {
                                        Some(*n)
                                    }

                                    (Tile::NorthWest, Direction::South | Direction::East) => {
                                        Some(*n)
                                    }
                                    (Tile::SouthWest, Direction::North | Direction::East) => {
                                        Some(*n)
                                    }
                                    (Tile::NorthEast, Direction::South | Direction::West) => {
                                        Some(*n)
                                    }
                                    (Tile::SouthEast, Direction::North | Direction::West) => {
                                        Some(*n)
                                    }
                                    (_, _) => None,
                                }
                            } else {
                                None
                            }
                        })
                        .collect();
                    graph.insert(coord, neighbors);
                }
            })
        });
        graph
    }

    fn find_loop(&self) -> Vec<Coord> {
        let graph = self.make_graph();
        let mut seq = vec![];
        let mut cur = self.start_tile;
        let mut next = graph.get(&self.start_tile).unwrap()[0];
        loop {
            seq.push(cur);
            if next == self.start_tile {
                break;
            }
            let neighbors = graph.get(&next).unwrap();
            if neighbors.len() != 2 {
                panic!("BROKEN PIPE!");
            }

            let new_next = if neighbors[0] == cur {
                neighbors[1]
            } else {
                neighbors[0]
            };
            cur = next;
            next = new_next;
        }
        seq
    }

    /// Removes all non-loop pipe tiles
    fn simplified(&self) -> Self {
        let pipe: HashSet<Coord> = self.find_loop().into_iter().collect();
        let mut tiles: Vec<Vec<Tile>> = self
            .tiles
            .iter()
            .enumerate()
            .map(|(y, row)| {
                row.iter()
                    .enumerate()
                    .map(|(x, tile)| {
                        if pipe.contains(&Coord::new(x, y)) {
                            tile.clone()
                        } else {
                            Tile::Empty
                        }
                    })
                    .collect()
            })
            .collect();
        let start_neighbors: Vec<Direction> = self
            .make_graph()
            .get(&self.start_tile)
            .expect("Start tile should exist")
            .iter()
            .map(|n| {
                if n.x == self.start_tile.x {
                    if n.y == self.start_tile.y - 1 {
                        Direction::North
                    } else {
                        Direction::South
                    }
                } else if n.x == self.start_tile.x - 1 {
                    Direction::West
                } else {
                    Direction::East
                }
            })
            .collect();
        let start_tile_type = match (start_neighbors[0], start_neighbors[1]) {
            (Direction::North, Direction::South) | (Direction::South, Direction::North) => {
                Tile::NorthSouth
            }
            (Direction::East, Direction::West) | (Direction::West, Direction::East) => {
                Tile::EastWest
            }
            (Direction::North, Direction::East) | (Direction::East, Direction::North) => {
                Tile::NorthEast
            }
            (Direction::North, Direction::West) | (Direction::West, Direction::North) => {
                Tile::NorthWest
            }
            (Direction::South, Direction::East) | (Direction::East, Direction::South) => {
                Tile::SouthEast
            }
            (Direction::South, Direction::West) | (Direction::West, Direction::South) => {
                Tile::SouthWest
            }

            _ => panic!("Wrong neighbors! {:?}", start_neighbors),
        };

        tiles[self.start_tile.y as usize][self.start_tile.x as usize] = start_tile_type;

        Day10 {
            start_tile: self.start_tile,
            tiles,
            graph: self.graph.clone(),
        }
    }
}

impl Display for Day10 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut display = String::new();
        for line in self.tiles.iter() {
            for tile in line {
                display.push(tile.into());
            }
            display.push('\n');
        }
        write!(f, "{}", display)
    }
}

impl Solver for Day10 {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut start_tile = Coord::new(0, 0);
        let tiles: Vec<Vec<Tile>> = input
            .lines()
            .enumerate()
            .map(|(row, line)| {
                line.chars()
                    .enumerate()
                    .map(|(col, ch)| match ch {
                        '|' => Tile::NorthSouth,
                        '-' => Tile::EastWest,
                        'L' => Tile::NorthEast,
                        'J' => Tile::NorthWest,
                        '7' => Tile::SouthWest,
                        'F' => Tile::SouthEast,
                        'S' => {
                            start_tile = Coord::new(col, row);
                            Tile::Start
                        }
                        _ => Tile::Empty,
                    })
                    .collect()
            })
            .collect();
        let mut solver = Day10 {
            start_tile,
            tiles,
            graph: None,
        };
        solver.graph = Some(solver.make_graph());
        // draw a pretty picture!
        println!("{}", solver);
        Ok(Box::new(solver))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let pipe = self.find_loop();
        Ok(pipe.len() / 2)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut simpled = self.simplified();
        println!("{}", simpled);
        enum LineState {
            Inside,
            Outside,
            InsideUp,
            InsideDown,
            OutsideUp,
            OutsideDown,
        }
        let mut total = 0;
        let mut state;
        let mut new_tiles = simpled.tiles.clone();
        for (y, row) in simpled.tiles.iter().enumerate() {
            state = LineState::Outside;
            for (x, tile) in row.iter().enumerate() {
                match (tile, &state) {
                    (Tile::Empty, LineState::Inside) => {
                        total += 1;
                        new_tiles[y][x] = Tile::Inside
                    }
                    (Tile::Empty, LineState::Outside) => new_tiles[y][x] = Tile::Outside,
                    // wall is definitely a change from in to out or vice versa
                    (Tile::NorthSouth, LineState::Outside) => state = LineState::Inside,
                    (Tile::NorthSouth, LineState::Inside) => state = LineState::Outside,
                    // keep track of inside/outside + direction the wall came in
                    (Tile::NorthEast, LineState::Outside) => state = LineState::OutsideUp,
                    (Tile::SouthEast, LineState::Outside) => state = LineState::OutsideDown,
                    (Tile::NorthEast, LineState::Inside) => state = LineState::InsideUp,
                    (Tile::SouthEast, LineState::Inside) => state = LineState::InsideDown,

                    // wall was up, leaving up, so no in/out swap
                    (Tile::NorthWest, LineState::InsideUp) => state = LineState::Inside,
                    (Tile::NorthWest, LineState::OutsideUp) => state = LineState::Outside,

                    // wall was up, leaving down, so swap
                    (Tile::SouthWest, LineState::InsideUp) => state = LineState::Outside,
                    (Tile::SouthWest, LineState::OutsideUp) => state = LineState::Inside,

                    // wall was down, leaving down, so no in/out swap
                    (Tile::SouthWest, LineState::InsideDown) => state = LineState::Inside,
                    (Tile::SouthWest, LineState::OutsideDown) => state = LineState::Outside,

                    // wall was down, leaving up, so swap
                    (Tile::NorthWest, LineState::InsideDown) => state = LineState::Outside,
                    (Tile::NorthWest, LineState::OutsideDown) => state = LineState::Inside,
                    _ => {}
                }
            }
        }

        simpled.tiles = new_tiles;
        println!("{}", simpled);
        Ok(total)
    }
}
